﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Win;
using DevExpress.Xpo;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Windows.Forms;
using System.Xml;
using TMA.Module.BusinessObjects.Management;

namespace TMA.Module.DatabaseUpdate
{
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.Updating.ModuleUpdater
    public class Updater : ModuleUpdater {

        private Airport newAirport = null;
        private Plane newPlane = null;

        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            CheckEmptyDatabase();
        }

        private void CheckEmptyDatabase()
        {
            if (IsEmptyDatabase())
                if (ShowDialogForFillDB() == DialogResult.Yes)
                    CreateDemoExampleData();
        }

        public bool IsEmptyDatabase()
        {
            Pilot theObject = ObjectSpace.FindObject<Pilot>(CriteriaOperator.Parse("[CountPlane] > ?", 0));
            return theObject == null;
        }

        public bool CreateDemoExampleData()
        {
            FileStream fs = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DemoData\Example.xml"), FileMode.Open, FileAccess.Read);
            XmlReaderSettings settings = new XmlReaderSettings();
            using (XmlReader reader = XmlReader.Create(fs, settings))
            {
                while (reader.Read())
                {
                    if(reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "airport":
                                CreateNewAirport(reader);
                                break;
                            case "plane":
                                CreateNewPlane(reader);
                                break;
                            case "pilot":
                                CreateNewPilot(reader);
                                break;
                            default:
                                break;
                        }
                        if (!CommitNewObj())
                            throw new UserFriendlyException("Загрузка данных прервана из-за ошибки.");
                    }
                }
            }
            return true;
        }

        private void CreateNewPilot(XmlReader reader)
        {
            string firstNamePilot = reader[0];
            string middleNamePilot = reader[1];
            string lastNamePilot = reader[2];
            string emailPilot = reader[3];
            DateTime birthDatePilot = DateTime.ParseExact(reader[4], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            Pilot pilotObj = ObjectSpace.FindObject<Pilot>(CriteriaOperator.Parse("FirstName=? && MiddleName=? && LastName=? && Email=?",
                                                                                  firstNamePilot, middleNamePilot, lastNamePilot, emailPilot));
            if (pilotObj == null)
            {
                pilotObj = ObjectSpace.CreateObject<Pilot>();
                pilotObj.FirstName = firstNamePilot;
                pilotObj.MiddleName = middleNamePilot;
                pilotObj.LastName = lastNamePilot;
                pilotObj.BirthDate = birthDatePilot;
                pilotObj.Email = emailPilot;
                pilotObj.Airport = newAirport;
                pilotObj.Photo = GetByteArrayImage();
            }
            var findPilots = from t in newPlane.Pilots.ToList<Pilot>()
                             where t.FirstName == pilotObj.FirstName
                                   && t.MiddleName == pilotObj.MiddleName
                                   && t.LastName == pilotObj.LastName
                                   && t.Email == pilotObj.Email
                             orderby t
                             select t;
            if (findPilots.Count<Pilot>() == 0)
                newAirport.Pilots.Add(pilotObj);

            var findPlanes = from t in pilotObj.Planes.ToList<Plane>()
                             where t.Name == newPlane.Name && t.Code == newPlane.Code
                             orderby t
                             select t;
            if (findPlanes.Count<Plane>() != 0)
                return;

            if (newPlane.Pilots.Count < 3) // Ограничение на количество пилотов у самолета
                newPlane.Pilots.Add(pilotObj);
        }

        private void CreateNewPlane(XmlReader reader)
        {
            string namePlane = reader[0];
            string codePlane = reader[1];
            Plane planeObj = ObjectSpace.FindObject<Plane>(CriteriaOperator.Parse("Name=? && Code=?", namePlane, codePlane));
            if (planeObj == null)
            {
                planeObj = ObjectSpace.CreateObject<Plane>();
                planeObj.Name = namePlane;
                planeObj.Code = codePlane;
                planeObj.Airport = newAirport;

                newAirport.Planes.Add(newPlane);
            }
            newPlane = planeObj;
        }

        private void CreateNewAirport(XmlReader reader)
        {
            string nameAirport = reader[0];
            Airport airportObj = ObjectSpace.FindObject<Airport>(CriteriaOperator.Parse("Name=?", nameAirport));
            if (airportObj == null)
            {
                airportObj = ObjectSpace.CreateObject<Airport>();
                airportObj.Name = nameAirport;

                newAirport = airportObj;
            }
        }

        private bool CommitNewObj()
        {
            try
            {
                ObjectSpace.CommitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private byte[] GetByteArrayImage()
        {
            string pathImage = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DemoData\Image\" + new Random().Next(1, 5).ToString() + ".jpg");
            Bitmap img = new Bitmap(pathImage);

            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        private DialogResult ShowDialogForFillDB()
        {
            return WinApplication.Messaging.GetUserChoice("База данных пустая!\nСгенерировать все исходные данные, необходимые для наиболее полной демонстрации работы приложения?", 
                "Внимание! Хороший вапрос!", 
                MessageBoxButtons.YesNo); ;
        }

        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }
    }
}
