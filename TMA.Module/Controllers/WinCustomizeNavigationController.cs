﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Xpo;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraEditors;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TMA.Module.BusinessObjects.Management;

namespace TMA.Module.Controllers
{
    public class WinCustomizeNavigationController : WindowController
    {
        private AccordionControl acControl;
        private static Session session;
        private static AccordionControlElement acRootItemSummary;

        public WinCustomizeNavigationController() 
        {
            session = new Session { ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString };
            session.Connect();
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.GetController<ShowNavigationItemController>().ShowNavigationItemAction.CustomizeControl += ShowNavigationItemAction_CustomizeControl;
        }

        private void ShowNavigationItemAction_CustomizeControl(object sender, CustomizeControlEventArgs e)
        {
            if (e.Control is AccordionControl control)
            {
                acControl = control;
                CreateNewAccordionControl();
            }
        }

        private void CreateNewAccordionControl()
        {
            acControl.BeginUpdate();
            CreateSummaryInfoPanel();
            CreateHyperlinkProject();
            acControl.EndUpdate();
        }

        private void CreateHyperlinkProject()
        {
            AccordionControlElement acHyperItemSettings = new AccordionControlElement();
            acHyperItemSettings.ImageOptions.ImageUri.Uri = "SvgImages/DiagramIcons/BindingEditorHelpIcon";
            acHyperItemSettings.Name = "hyperlink";
            acHyperItemSettings.Style = ElementStyle.Item;
            acHyperItemSettings.Text = "Проект";

            HyperlinkLabelControl hyperlinkLabelControl1 = new HyperlinkLabelControl()
            {
                Location = new Point(26, 10),
                Size = new Size(107, 13),
                Text = "<href=https://bitbucket.org/terekhov_m/myexampleprojectxaf/src/master/>Bitbucket</href>"
            };

            hyperlinkLabelControl1.HyperlinkClick += this.HyperlinkLabelControl1_HyperlinkClick;

            AccordionContentContainer itemSettingsControlContainer = new AccordionContentContainer();
            itemSettingsControlContainer.Appearance.BackColor = SystemColors.Control;
            itemSettingsControlContainer.Appearance.Options.UseBackColor = true;
            itemSettingsControlContainer.Height = 60;
            itemSettingsControlContainer.Controls.Add(hyperlinkLabelControl1);
            
            acHyperItemSettings.ContentContainer = itemSettingsControlContainer;

            acControl.Controls.Add(itemSettingsControlContainer);
            acControl.Elements.Add(acHyperItemSettings);
        }

        private void CreateSummaryInfoPanel()
        {
            acRootItemSummary = new AccordionControlElement();
            acRootItemSummary.ImageOptions.ImageUri.Uri = "SvgImages/Business Objects/BO_PivotChart";
            acRootItemSummary.Name = "summaryInfo";
            acRootItemSummary.Style = ElementStyle.Item;
            acRootItemSummary.Text = "Сводная информация";
            acRootItemSummary.Click += AcRootItemSummary_Click;

            AccordionContentContainer accordionContentContainer = new AccordionContentContainer() { Height = 70 };
            acRootItemSummary.ContentContainer = accordionContentContainer;

            acControl.Controls.Add(accordionContentContainer);
            acControl.Elements.Add(acRootItemSummary);

            UpdateSummaryPanel();
        }

        private AccordionContentContainer CreateAndUpdateItemSummaryContainer()
        {
            LabelControl labelCountAirports = new LabelControl()
            {
                Location = new Point(26, 10),
                Size = new Size(70, 13),
                Text = string.Format("Всего аэропортов: {0} шт.", GetSummaryCount(typeof(Airport)).ToString())
            };
            LabelControl labelCountPlanes = new LabelControl()
            {
                Location = new Point(26, 30),
                Size = new Size(70, 13),
                Text = string.Format("Всего самолетов: {0} шт.", GetSummaryCount(typeof(Plane)).ToString())
            };
            LabelControl labelCountPilots = new LabelControl()
            {
                Location = new Point(26, 50),
                Size = new Size(70, 13),
                Text = string.Format("Всего пилотов: {0} шт.", GetSummaryCount(typeof(Pilot)).ToString())
            };

            AccordionContentContainer itemContainer = new AccordionContentContainer { Height = 70 };
            itemContainer.Controls.AddRange(new Control[] { labelCountAirports, labelCountPlanes, labelCountPilots });

            return itemContainer;
        }

        private void AcRootItemSummary_Click(object sender, EventArgs e)
        {
            UpdateSummaryPanel();
        }

        public void UpdateSummaryPanel()
        {
            RemoveItemsToAccordionControl(acRootItemSummary.ContentContainer.Controls);
            acRootItemSummary.ContentContainer.Controls.Add(CreateAndUpdateItemSummaryContainer());
        }

        private void RemoveItemsToAccordionControl(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                controls.Remove(control);
            }
        }

        private int GetSummaryCount(Type type)
        {
            int elCount = 0;
            if (type == typeof(Airport))
            {
                XPQuery<Airport> airport = session.Query<Airport>();
                var list = from c in airport
                           select c.Oid;
                elCount = list.Count();
            }
            else if(type == typeof(Plane))
            {
                XPQuery<Plane> plane = session.Query<Plane>();
                var list = from c in plane
                           select c.Oid;
                elCount = list.Count();
            }
            else if (type == typeof(Pilot))
            {
                XPQuery<Pilot> pilot = session.Query<Pilot>();
                var list = from c in pilot
                           select c.Oid;
                elCount = list.Count();
            }

            return elCount;
        }

        private void HyperlinkLabelControl1_HyperlinkClick(object sender, DevExpress.Utils.HyperlinkClickEventArgs e)
        {
            Process.Start(e.Link);
        }

        protected override void OnDeactivated()
        {
            Frame.GetController<ShowNavigationItemController>().ShowNavigationItemAction.CustomizeControl -=
                ShowNavigationItemAction_CustomizeControl;
            base.OnDeactivated();
            session.Disconnect();
            session.Dispose();
        }
    }
}
