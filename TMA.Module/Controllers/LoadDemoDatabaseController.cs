﻿using DevExpress.DataAccess.Sql;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Win.Core;
using DevExpress.Xpo.DB.Helpers;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using System;
using System.Windows.Forms;

namespace TMA.Module.Controllers
{
    public class LoadDemoDatabaseController : ViewController
    {
        public LoadDemoDatabaseController()
        {
            SimpleActionExecuteEventHandler simpleActionExecuteEventHandler = LoadDemoDatabaseAction;
            SimpleAction action1 = new SimpleAction(this, "ActionLoadDemoDatabaseAction", "View", simpleActionExecuteEventHandler)
            {
                Caption = "Загрузить демонстрационные данные",
                ImageName = "UpdateDataExtract"
            };
        }

        private void LoadDemoDatabaseAction(Object sender, SimpleActionExecuteEventArgs e)
        {
            DatabaseUpdate.Updater updater = new DatabaseUpdate.Updater(ObjectSpace, new Version("0.0.0.0"));
            bool successUpdate;
            if (updater.IsEmptyDatabase())
            {
                successUpdate = updater.CreateDemoExampleData();
                ShowDialogSuccessAndError(successUpdate);
            }
            else { ShowDialogClearDatabase(); }
        }

        private void ShowDialogSuccessAndError(bool successUpdate)
        {
            Messaging messaging = new Messaging(Application);
            if (successUpdate)
                messaging.Show("Демонстрационные данные успешно загружены.", "Успешно!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                messaging.Show("Демонстрационные данные не загружены. Исправьте ошибки и повторите попытку.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ShowDialogClearDatabase()
        {
            Messaging messaging = new Messaging(Application);
            messaging.Show("База данных уже заполнена!\nПожалуйста очистите базу данных и повторите попытку снова.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
