﻿using DevExpress.ExpressApp.SystemModule;

namespace TMA.Module.Controllers
{
    public class CustomRefreshController : RefreshController
    {
        public CustomRefreshController() { }

        protected override void Refresh()
        {
            base.Refresh();
            WinCustomizeNavigationController winCustomizeNavigationController = new WinCustomizeNavigationController();
            winCustomizeNavigationController.UpdateSummaryPanel();
        }
    }
}
