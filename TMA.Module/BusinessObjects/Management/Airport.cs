﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System.ComponentModel;

namespace TMA.Module.BusinessObjects.Management
{
    [NavigationItem("Management"), System.ComponentModel.DisplayName("Аэропорты"), ImageName("BO_Position_v92")]
    public class Airport : XPObject
    {
        public Airport(Session session) : base(session) { }

        [Browsable(false)]
        public int Id
        {
            get => id;
            protected set { SetPropertyValue(nameof(Id), ref id, value); }
        }
        int id;

        [DevExpress.Xpo.DisplayName("Наименование"), FieldSize(FieldSizeAttribute.Unlimited)]
        [RuleRequiredField("RuleRequiredField for NameAirport", DefaultContexts.Save, "Не заполнено поле \"Наименование\"")]
        public string Name
        {
            get => name;
            set { SetPropertyValue(nameof(Name), ref name, value); }
        }
        string name;

        [Association("Airport-Pilot"), DevExpress.Xpo.DisplayName("Пилоты"), DevExpress.Xpo.Aggregated]
        public XPCollection<Pilot> Pilots
        {
            get => GetCollection<Pilot>(nameof(Pilots));
        }

        [Association("Airport-Plane"), DevExpress.Xpo.DisplayName("Самолеты"), DevExpress.Xpo.Aggregated]
        public XPCollection<Plane> Planes
        {
            get => GetCollection<Plane>(nameof(Planes));
        }

        [DevExpress.Xpo.DisplayName("Количество пилотов"), FieldSize(FieldSizeAttribute.Unlimited)]
        [PersistentAlias("Pilots.Count")]
        public int CountPilots
        {
            get => Pilots.Count;
        }

        [DevExpress.Xpo.DisplayName("Количество самолетов"), FieldSize(FieldSizeAttribute.Unlimited)]
        [PersistentAlias("Planes.Count")]
        public int CountPlanes
        {
            get => Planes.Count;
        }
    }
}
