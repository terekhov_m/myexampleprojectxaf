﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using TMA.Module.Controllers;
using DisplayNameAttribute = DevExpress.Xpo.DisplayNameAttribute;

namespace TMA.Module.BusinessObjects.Management
{
    [NavigationItem("Management"), System.ComponentModel.DisplayName("Пилоты"), ImageName("BO_Department")]
    [DefaultClassOptions]
    public class Pilot : XPObject
    {
        public Pilot(Session session) : base(session) { }

        [Browsable(false)]
        public int Id
        {
            get => id;
            protected set { SetPropertyValue(nameof(Id), ref id, value); }
        }
        int id;

        [RuleRequiredField("RuleRequiredField for FirstName", DefaultContexts.Save, "Не заполнено поле \"Имя\"")]
        [DisplayName("Имя"), FieldSize(50), Size(50)]
        public string FirstName
        {
            get => firstName;
            set { SetPropertyValue<string>("FirstName", ref firstName, value); }
        }
        string firstName;

        [RuleRequiredField("RuleRequiredField for MiddleName", DefaultContexts.Save, "Не заполнено поле \"Отчество\"")]
        [DisplayName("Отчество"), FieldSize(50), Size(50)]
        public string MiddleName
        {
            get => middleName;
            set { SetPropertyValue<string>("MiddleName", ref middleName, value); }
        }
        string middleName;

        [RuleRequiredField("RuleRequiredField for LastName", DefaultContexts.Save, "Не заполнено поле \"Фамилия\"")]
        [DisplayName("Фамилия"), FieldSize(50), Size(50)]
        public string LastName
        {
            get => lastName;
            set { SetPropertyValue<string>("LastName", ref lastName, value); }
        }
        string lastName;

        [RuleRequiredField("RuleRequiredField for Email", DefaultContexts.Save, "Поле \"Email\" не соответсвует формату")]
        [RuleRegularExpression(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$")]
        public string Email
        {
            get => email;
            set { SetPropertyValue<string>("Email", ref email, value); }
        }
        string email;

        [RuleRequiredField("RuleRequiredField for BirthDate", DefaultContexts.Save, "Не заполнено поле \"Дата рождения\"")]
        [DisplayName("Дата рождения")]
        public DateTime BirthDate
        {
            get => birthDate;
            set { SetPropertyValue<DateTime>("BirthDate", ref birthDate, value); }
        }
        DateTime birthDate;

        [NotMapped]
        [DisplayName("ФИО"), FieldSize(150)]
        public string FullName
        {
            get => string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
            set { SetPropertyValue(nameof(FullName), ref fullName, value); }
        }
        private string fullName;

        [ImageEditor(ListViewImageEditorCustomHeight = 75, DetailViewImageEditorFixedHeight = 150)]
        [DisplayName("Фото")]
        public byte[] Photo
        {
            get => photo;
            set { SetPropertyValue<byte[]>("Photo", ref photo, value); }
        }
        byte[] photo;

        [DisplayName("Самолеты")]
        [Association("Plane-Pilot")]
        public XPCollection<Plane> Planes
        {
            get => GetCollection<Plane>(nameof(Planes));
        }

        [Browsable(false)]
        [Association("Airport-Pilot")]
        public Airport Airport
        {
            get => airport;
            set { SetPropertyValue(nameof(Airport), ref airport, value); }
        }
        private Airport airport;

        [NotMapped]
        [DisplayName("Количество самолетов")]
        [Calculated("Planes.Count")] // ???
        [PersistentAlias("Planes.Count")]
        public int CountPlane
        {
            get => Planes.Count;
        }

        [NotMapped]
        [DisplayName("Количество \"зеленых\" самолетов")]
        public int CountPlaneGreen
        {
            get
            {
                var selectedTeams = from t in Planes where t.IsStrGreen orderby t select t;
                return selectedTeams.Count<Plane>();
            }
        }

        [Action(ToolTip = "OpenMainReport", Caption = "Открыть основной отчет", ImageName = "BO_KPI_Scorecard")]
        public void OpenMainReport()
        {
            try
            {
                Pilot currentPilot = (Pilot)this;
                if (currentPilot != null)
                {
                    MainReport report = new MainReport()
                    {
                        DataSource = CreateDataSourceForReport()
                    };

                    using (ReportPrintTool printTool = new ReportPrintTool(report))
                    {
                        printTool.ShowPreviewDialog();
                        printTool.Report.CreateDocument();
                    }
                }
            }
            catch
            {
                throw new UserFriendlyException("Ошибка при открытии отчет");
            }
        }

        [Action(ToolTip = "OpenMainSimpleReport", Caption = "Открыть основной отчет(упращенный)", ImageName = "BO_Report")]
        public void OpenMainSimpleReport()
        {
            try
            {
                Pilot currentPilot = this;
                if (currentPilot != null)
                {
                    MainSimpleReport report = new MainSimpleReport()
                    {
                        DataSource = CreateDataSourceForReport()
                    };
                    using (ReportPrintTool printTool = new ReportPrintTool(report))
                    {
                        printTool.ShowPreviewDialog();
                        printTool.Report.CreateDocument();
                    }
                }
            }
            catch
            {
                throw new UserFriendlyException("Ошибка при открытии отчета.");
            }
        }

        private List<Pilot> CreateDataSourceForReport()
        { 
            return new List<Pilot> { this };
        }
    }
}
