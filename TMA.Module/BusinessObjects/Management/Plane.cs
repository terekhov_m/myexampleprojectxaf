﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System.ComponentModel;
using System.Drawing;
using DisplayNameAttribute = DevExpress.Xpo.DisplayNameAttribute;

namespace TMA.Module.BusinessObjects.Management
{
    [NavigationItem("Management"), System.ComponentModel.DisplayName("Самолеты"), ImageName("Travel_Plane")]
    [Appearance("GreenStringPlanes", AppearanceItemType = "ViewItem", TargetItems = "*", 
        Criteria = "IsStrGreen", Context = "ListView", FontStyle = FontStyle.Bold, FontColor = "Green", Priority = 1)]
    public class Plane : XPObject
    {
        public Plane(Session session) : base(session) { }

        [Browsable(false)]
        public int Id
        {
            get => id;
            protected set { SetPropertyValue(nameof(Id), ref id, value); }
        }
        int id;

        [DevExpress.Xpo.DisplayName("Наименование"), FieldSize(200), Size(199)]
        [RuleRequiredField("RuleRequiredField for NamePlane", DefaultContexts.Save, "Не заполнено поле \"Наименование\"")]
        public string Name
        {
            get => name;
            set { SetPropertyValue(nameof(Name), ref name, value); }
        }
        string name;

        [DevExpress.Xpo.DisplayName("Код"), FieldSize(FieldSizeAttribute.Unlimited)]
        [RuleRequiredField("RuleRequiredField for Code", DefaultContexts.Save, "Не заполнено поле \"Код\"")]
        public string Code
        {
            get => code;
            set { SetPropertyValue(nameof(Code), ref code, value); }
        }
        string code;

        [Browsable(false)]
        public bool IsStrGreen
        {
            get
            {
                if (Code.IndexOf('А') > -1 || Code.IndexOf('A') > -1)
                    return true;
                else
                    return false;
            }
        }

        [Association("Plane-Pilot"), DisplayName("Пилоты"), ImmediatePostData(true)]
        public XPCollection<Pilot> Pilots
        {
            get => GetCollection<Pilot>(nameof(Pilots));
        }

        [Association("Airport-Plane")]
        [Browsable(false)]
        public Airport Airport
        {
            get => airport;
            set { SetPropertyValue(nameof(Airport), ref airport, value); }
        }
        private Airport airport;

        [DevExpress.Xpo.DisplayName("Количество пилотов"), ImmediatePostData(true)]
        public int CountPilots
        {
            get => Pilots.Count;
        }

        [Browsable(false)]
        [RuleFromBoolProperty("Valid List Pilots", DefaultContexts.Save, "Максимально допустимое количество пилотов - 3")]
        public bool ValidListPilots
        {
            get => Pilots.Count <= 3;
        }

        [Association("Plane-Flight"), DisplayName("Рейсы")]
        public XPCollection<Flight> Flights
        {
            get => GetCollection<Flight>(nameof(Flights));
        }
    }
}
