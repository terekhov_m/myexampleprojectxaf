﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Win;
using DevExpress.ExpressApp.Win.Core;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Windows.Forms;
using AssociationAttribute = DevExpress.Xpo.AssociationAttribute;
using DisplayNameAttribute = DevExpress.Xpo.DisplayNameAttribute;

namespace TMA.Module.BusinessObjects.Management
{
    [NavigationItem("Management"), System.ComponentModel.DisplayName("Рейсы"), ImageName("SparklineLine")]
    [Appearance("Status0", AppearanceItemType = "ViewItem", TargetItems = "*", Criteria = "Status==0", Context = "ListView", FontStyle = FontStyle.Bold, BackColor = "#e0e0e0", FontColor = "Black", Priority = 1)]
    [Appearance("Status1", AppearanceItemType = "ViewItem", TargetItems = "*", Criteria = "Status==1", Context = "ListView", FontStyle = FontStyle.Bold, BackColor = "#9ccc65", FontColor = "White", Priority = 1)]
    [Appearance("Status2", AppearanceItemType = "ViewItem", TargetItems = "*", Criteria = "Status==2", Context = "ListView", FontStyle = FontStyle.Bold, BackColor = "#4fc3f7", FontColor = "White", Priority = 1)]
    [Appearance("Status3", AppearanceItemType = "ViewItem", TargetItems = "*", Criteria = "Status==3", Context = "ListView", FontStyle = FontStyle.Bold, BackColor = "#ef5350", FontColor = "White", Priority = 1)]
    [Appearance("Status4", AppearanceItemType = "ViewItem", TargetItems = "*", Criteria = "Status==3", Context = "ListView", FontStyle = FontStyle.Strikeout, Priority = 1)]
    public class Flight : XPObject
    {
        public Flight(Session session) : base(session) { }

        [DisplayName("Время отправления"), FieldSize(FieldSizeAttribute.Unlimited)]
        [RuleRequiredField("RuleRequiredField for DepartureDate", DefaultContexts.Save, "Не заполнено поле \"Время отправления\"")]
        [DateTimeEditMask("yyyy-MM-dd HH:mm:ss", UseMaskAsDisplayFormat = false)]
        public DateTime DepartureDate
        {
            get => departureDate;
            set { SetPropertyValue(nameof(DepartureDate), ref departureDate, value); }
        }
        private DateTime departureDate;

        [DisplayName("Время прибытия"), FieldSize(FieldSizeAttribute.Unlimited)]
        [RuleRequiredField("RuleRequiredField for ArrivalDate", DefaultContexts.Save, "Не заполнено поле \"Время прибытия\"")]
        [DateTimeEditMask("yyyy-MM-dd HH:mm:ss", UseMaskAsDisplayFormat = true)]
        public DateTime ArrivalDate
        {
            get => arrivalDate;
            set { SetPropertyValue(nameof(ArrivalDate), ref arrivalDate, value); }
        }
        private DateTime arrivalDate;

        [DisplayName("Аэропорт отправления")]
        [RuleRequiredField("RuleRequiredField for DepartureCity", DefaultContexts.Save, "Не заполнено поле \"Аэропорт отправления\"")]
        public Airport DepartureAirport
        {
            get => departureAirport;
            set { SetPropertyValue(nameof(DepartureAirport), ref departureAirport, value); }
        }
        private Airport departureAirport;

        [DisplayName("Аэропорт прибытия")]
        [RuleRequiredField("RuleRequiredField for ArrivalCity", DefaultContexts.Save, "Не заполнено поле \"Аэропорт прибытия\"")]
        public Airport ArrivalAirport
        {
            get => arrivalAirport;
            set { SetPropertyValue(nameof(ArrivalAirport), ref arrivalAirport, value); }
        }
        private Airport arrivalAirport;

        [Association("Plane-Flight"), DisplayName("Самолет")]
        [RuleRequiredField("RuleRequiredField for PlaneFlight", DefaultContexts.Save, "Не заполнено поле \"Самолет\"")]
        public Plane PlaneFlight
        {
            get => planeFlight;
            set { SetPropertyValue(nameof(PlaneFlight), ref planeFlight, value); }
        }
        private Plane planeFlight;

        [DisplayName("Пилоты")]
        public XPCollection<Pilot> Pilots
        {
            get => PlaneFlight?.Pilots;
        }

        [Browsable(false)]
        public StatusType Status
        {
            get
            {
                StatusType _status = StatusType.NotDefined;
                if (!isFlightCancelled)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if (currentDateTime < DepartureDate) { _status = StatusType.Expectation; }
                    else if (currentDateTime >= DepartureDate && currentDateTime < ArrivalDate) { _status = StatusType.Arrived; }
                    else if (currentDateTime >= ArrivalDate) { _status = StatusType.Departed; }
                }
                else
                {
                    _status = StatusType.Excellent;
                }
                return _status;
            }
        }

        [Browsable(false)]
        public bool IsFlightCancelled
        {
            get => isFlightCancelled;
            set { SetPropertyValue(nameof(IsFlightCancelled), ref isFlightCancelled, value); }
        }
        private bool isFlightCancelled;

        [DisplayName("Статус")]
        public string StatusDisplay
        {
            get
            {
                string _statusDisplay = "Не определен";
                if (Status == StatusType.Expectation)
                {
                    _statusDisplay = "В ожедании вылета";
                }
                else if(Status == StatusType.Arrived)
                {
                    _statusDisplay = "В пути";
                }
                else if(Status == StatusType.Departed)
                {
                    _statusDisplay = "Прибыл";
                }
                else if(Status == StatusType.Excellent)
                {
                    _statusDisplay = "Отменен";
                }
                return _statusDisplay;
            }
        }

        [Action(ToolTip = "CancelledFlight", Caption = "Отменить рейс", ImageName = "FollowUp")]
        public void CancelledFlightAction()
        {
            try
            {
                if(Status == 0)
                {
                    this.BeginEdit();
                    IsFlightCancelled = true;
                    this.EndEdit();
                    this.Save();
                }
                else
                {
                    WinApplication.Messaging.GetUserChoice("Данный запрещен для отмены.", "Запрещено", MessageBoxButtons.OK);
                }
            }
            catch
            {
                throw new UserFriendlyException("Ошибка при открытии отчета.");
            }
        }

        public enum StatusType
        {
            Expectation, // Ожедание отправления
            Departed,    // В пути
            Arrived,     // Прибыл
            Excellent,   // Вылет отменен
            NotDefined   // Не определен
        }
    }
}
