﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using System;
using System.Collections.Generic;
using TMA.Module.BusinessObjects.Management;
using TMA.Module.Controllers;

namespace TMA.Module
{
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.ModuleBase.
    public sealed partial class TMAModule : ModuleBase {
        public TMAModule() {
            InitializeComponent();
        }
        public override IEnumerable<ModuleUpdater> GetModuleUpdaters(IObjectSpace objectSpace, Version versionFromDB) {
            ModuleUpdater updater = new DatabaseUpdate.Updater(objectSpace, versionFromDB);

            PredefinedReportsUpdater predefinedReportsUpdater = new PredefinedReportsUpdater(Application, objectSpace, versionFromDB);
            // Main report
            predefinedReportsUpdater.AddPredefinedReport<MainReport>("Основной отчет", typeof(Pilot));
            // Main simple report
            predefinedReportsUpdater.AddPredefinedReport<MainSimpleReport>("Основной отчет (упращенный)", typeof(Pilot));

            return new ModuleUpdater[] { updater, predefinedReportsUpdater };
        }
        public override void Setup(XafApplication application) {
            base.Setup(application);
            // Manage various aspects of the application UI and behavior at the module level.
        }
        public override void CustomizeTypesInfo(ITypesInfo typesInfo) {
            base.CustomizeTypesInfo(typesInfo);
            CalculatedPersistentAliasHelper.CustomizeTypesInfo(typesInfo);
            
            // Hide OID to Pilots
            var oid = typesInfo.FindTypeInfo(typeof(Pilot)).FindMember("Oid");
            if (oid != null)
            {
                oid.AddAttribute(new System.ComponentModel.BrowsableAttribute(false));
            }
        }
    }
}
